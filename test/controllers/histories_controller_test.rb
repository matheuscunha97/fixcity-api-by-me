# == Schema Information
#
# Table name: histories
#
#  id                :integer          not null, primary key
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  date              :datetime
#  number_solved     :integer
#  number_registered :integer
#

require 'test_helper'

class HistoriesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

end
