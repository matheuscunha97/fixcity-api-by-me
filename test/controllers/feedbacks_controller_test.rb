# == Schema Information
#
# Table name: feedbacks
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_feedbacks_on_user_id  (user_id)
#

require 'test_helper'

class FeedbacksControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

end
