# == Schema Information
#
# Table name: ocurrences
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#  type_id    :integer
#
# Indexes
#
#  index_ocurrences_on_type_id  (type_id)
#  index_ocurrences_on_user_id  (user_id)
#

require 'test_helper'

class OcurrencesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

end
