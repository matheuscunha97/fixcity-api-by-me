# == Schema Information
#
# Table name: users
#
#  id           :integer          not null, primary key
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  password     :string
#  email        :string
#  cpf          :string
#  phone        :string
#  neighborhood :string
#  street       :string
#  number       :string
#  state        :string
#  type         :boolean
#  enabled      :boolean
#

require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

end
