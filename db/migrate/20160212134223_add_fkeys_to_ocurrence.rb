class AddFkeysToOcurrence < ActiveRecord::Migration
  def change
    add_reference :ocurrences, :user, index: true, foreign_key: true
    add_reference :ocurrences, :type, index: true, foreign_key: true
  end
end
