class AddFieldsToType < ActiveRecord::Migration
  def change
    add_column :types, :name, :string
    add_column :types, :description, :string
  end
end
