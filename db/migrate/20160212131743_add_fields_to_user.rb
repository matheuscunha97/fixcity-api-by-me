class AddFieldsToUser < ActiveRecord::Migration
  def change
    add_column :users, :password, :string
    add_column :users, :email, :string
    add_column :users, :cpf, :string
    add_column :users, :phone, :string
    add_column :users, :neighborhood, :string
    add_column :users, :street, :string
    add_column :users, :number, :string
    add_column :users, :state, :string
    add_column :users, :type, :boolean
    add_column :users, :enabled, :boolean
  end
end
