class AddFieldsToHistory < ActiveRecord::Migration
  def change
    add_column :histories, :date, :datetime
    add_column :histories, :number_solved, :integer
    add_column :histories, :number_registered, :integer
  end
end
