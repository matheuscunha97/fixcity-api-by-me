# == Schema Information
#
# Table name: users
#
#  id           :integer          not null, primary key
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  password     :string
#  email        :string
#  cpf          :string
#  phone        :string
#  neighborhood :string
#  street       :string
#  number       :string
#  state        :string
#  type         :boolean
#  enabled      :boolean
#

module UsersHelper
end
