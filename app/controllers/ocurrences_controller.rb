# == Schema Information
#
# Table name: ocurrences
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#  type_id    :integer
#
# Indexes
#
#  index_ocurrences_on_type_id  (type_id)
#  index_ocurrences_on_user_id  (user_id)
#

class OcurrencesController < ApplicationController
  def index
  	@ocurrences = Ocurrence.all
  end

  def new
  	@ocurrence = Ocurrence.new
  end

  def create
  	@ocurrence = Ocurrence.new params[:ocurrence]
  	if @ocurrence.save
  		redirect_to ocurrences_path
	else
		render :action => :new
	end
  end

  def edit
  	@ocurrence = Ocurrence.find params[:id]
  end

  def update
  	@ocurrence = Ocurrence.find params[:id]
  	if @ocurrence.update_attributes params[:ocurrence]
  		redirect_to ocurrences_path
  	else
  		render :action => :edit
  	end
  end

  def destroy
  	@ocurrence = Ocurrence.find params[:id]
  	@ocurrence.destroy
  	redirect_to ocurrences_path
  end

  def show
  	@ocurrence = Ocurrence.find params[:id]
  end
end
