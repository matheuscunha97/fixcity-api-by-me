# == Schema Information
#
# Table name: histories
#
#  id                :integer          not null, primary key
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  date              :datetime
#  number_solved     :integer
#  number_registered :integer
#

class HistoriesController < ApplicationController
  def index
  	@histories = History.all
  end

  def new
  	@history = History.new
  end

  def create
  	@history = History.new params[:histories]
  	if @history.save
  		redirect_to histories_path
  	else
  		render :action => :new
  	end
  end

  def edit
  	@history = History.find params[:id]
  end

  def update
  	@history = History.find params[:id]
  	if @history.update_attributes params[:histories]
  		redirect_to histories_path
  	else
  		render :action => :edit
  	end
  end

  def destroy
  	@history = History.find params[:id]
  	@history.destroy
  	redirect_to histories_path
  end

  def show
  	@history = History.find	params[:id]
  end
end
