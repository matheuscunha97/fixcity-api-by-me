# == Schema Information
#
# Table name: users
#
#  id           :integer          not null, primary key
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  password     :string
#  email        :string
#  cpf          :string
#  phone        :string
#  neighborhood :string
#  street       :string
#  number       :string
#  state        :string
#  type         :boolean
#  enabled      :boolean
#

class UsersController < ApplicationController
  def index
  	@users = User.all.where('enabled = false')
  end

  def new
  	@user = User.new
  end

  def create
  	@user = User.new params[:user]
  	if @user.save
  		redirect_to users_path
  	else
  		render :action => :new
  	end
  end

  def edit
  	@user = User.find params[:id]
  end

  def update
  	@user = User.find params[:id]
  	if @user.update_attributes params[:user]
  		redirect_to users_path
  	else
  		render :action => :edit
 	  end
  end

  def destroy
  	@user = User.find params[:id]
  	@user.destroy
  	redirect_to users_path
  end

  def show
  	@user = User.find params[:id]
  end

end
