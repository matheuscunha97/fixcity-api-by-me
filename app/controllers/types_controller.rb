# == Schema Information
#
# Table name: types
#
#  id          :integer          not null, primary key
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  name        :string
#  description :string
#

class TypesController < ApplicationController
  def index
  	@types = Type.all
  end

  def new
  	@type = Type.new
  end

  def create
  	@type = Type.new params[:user]
  	if @type.save
  	   redirect_to types_path
  	else
  	   render :action => :new
  	end
  end

  def edit
  	@type = Type.find params[:id]
  end

  def update
  	@type = Type.find params[:id]
  	if @type.update_attributes params[:type]
  		redirect_to types_path
  	else
  		render :action => :edit
  	end
  end

  def destroy
  	@type = Type.find params[:id]
  	@type.destroy
  	redirect_to types_path
  end

  def show
  	@type = Type.find params[:id]
  end
end
