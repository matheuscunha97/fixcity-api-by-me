# == Schema Information
#
# Table name: feedbacks
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_feedbacks_on_user_id  (user_id)
#

class FeedbacksController < ApplicationController
  def index
  	@feedbacks = Feedback.all
  end

  def new
  	@feedback = Feedback.new
  end

  def create
  	@feedback = Feedback.new params[:feedbacks]
  	if @feedback.save
  		redirect_to feedbacks_path
  	else
  		render :action => :new
  	end
  end

  def edit
  	@feedback = Feedback.find params[:id]
  end

  def update
  	@feedback = Feedback.find params[:id]
  	if @feedback.update_attributes params[:feedbacks]
  		redirect_to feedbacks_path
  	else
  		render :action => :edit
  	end
  end

  def destroy
  	@feedback = Feedback.find params[:id]
  	@feedback.destroy
  	redirect_to feedbacks_path
  end

  def show
  	@feedback = Feedback.find params[:id]
  end
end
